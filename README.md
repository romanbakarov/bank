
## Run with Virtual Environments


Create virtual environments
```
bash
virtualenv --python=python3.8 venv
source venv/bin/activate
```


Install pre-configured requirements
```
bash
pip install -r requirements.txt
```

Create .env file in src directory
```
bash
touch src/.env
```

Add config to the file (see `.env.example` file)

Add an environment variable to read from `.env` file

```
bash
export DJANGO_READ_DOT_ENV_FILE=true
```

## Run with Docker Compose

```
docker-compose -f docker-compose-develop.yml up
```

or

```
export COMPOSE_FILE=docker-compose-develop.yml
docker-compose up
```