from django.contrib import admin

# Register your models here.
from bank_accounts import models

admin.site.register(models.BankAccount)
admin.site.register(models.TransferHistory)
