from django.urls import path
from rest_framework.routers import DefaultRouter

from bank_accounts import views

router = DefaultRouter()
router.register(r'bank_accounts', views.BankAccountsViewSet, basename='bank_accounts')
router.register(r'transfer_history', views.TransferHistoryAPIView, basename='transfer_history')

urlpatterns = [
    path('make_transfer/', views.TransferAPIView.as_view(), name='make_transfer')
]
urlpatterns += router.urls
