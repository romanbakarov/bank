from django.db.models import Q

from rest_framework import viewsets, generics, mixins

from bank_accounts import serializers, models


class BankAccountsViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.BankAccountSerializer
    queryset = models.BankAccount.objects.all()

    def get_queryset(self):
        return super().get_queryset().filter(
            customer=self.request.user)


class TransferAPIView(generics.CreateAPIView):
    serializer_class = serializers.TransfersSerializer
    queryset = models.BankAccount.objects.all()


class TransferHistoryAPIView(mixins.RetrieveModelMixin,
                             mixins.ListModelMixin,
                             viewsets.GenericViewSet):
    serializer_class = serializers.TransfersHistorySerializer
    queryset = models.TransferHistory.objects.all()

    def get_queryset(self):
        return super().get_queryset().filter(
            Q(sender__customer=self.request.user) |
            Q(receiver__customer=self.request.user)
        )
