from rest_framework import serializers

from bank_accounts import models, utils, validators


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BankAccount
        fields = ('id', 'customer', 'balance')


class TransfersSerializer(serializers.Serializer):
    sender = serializers.PrimaryKeyRelatedField(queryset=models.BankAccount.objects.all())
    receiver = serializers.PrimaryKeyRelatedField(queryset=models.BankAccount.objects.all())
    amount = serializers.IntegerField()

    class Meta:
        fields = ('sender', 'receiver', 'amount')
        validators = [
            validators.SenderBankAccountBelongsToRequestCustomerValidator(),
            validators.SenderBankAccountNotEqualsReceiverBankAccountValidator(),
            validators.BankAccountBalanceCannotBeNegativeValidator(),
        ]

    def create(self, validated_data):
        sender = validated_data.get('sender')
        receiver = validated_data.get('receiver')
        amount = validated_data.get('amount')
        sender.make_transfer(receiver, amount)
        utils.write_transfer_history(sender, receiver, amount)
        self._data = {
            "message": f"{amount} was send successfully to {receiver.customer.username}"
        }
        return self._data


class TransfersHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TransferHistory
        fields = ('sender', 'receiver', 'amount', 'created_at')
