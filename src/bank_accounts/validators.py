from rest_framework import serializers


class SenderBankAccountBelongsToRequestCustomerValidator:
    requires_context = True

    def __call__(self, attrs, serializer):
        if attrs.get('sender', None) is not None:
            if attrs['sender'].customer != serializer.context['request'].user:
                raise serializers.ValidationError(
                    {"Error": "Sender bank account doesn't belongs to current customer"}
                )


class SenderBankAccountNotEqualsReceiverBankAccountValidator:
    requires_context = True

    def __call__(self, attrs, serializer):
        if attrs.get('sender', None) is not None and attrs.get('receiver', None) is not None:
            if attrs['sender'] == attrs['receiver']:
                raise serializers.ValidationError(
                    {"Error": "You can't transfer to to himself"}
                )


class BankAccountBalanceCannotBeNegativeValidator:
    requires_context = True

    def __call__(self, attrs, serializer):
        if attrs.get('sender', None) is not None:
            if attrs['sender'].balance - attrs['amount'] < 0:
                raise serializers.ValidationError(
                    {"Error": "You don't have enough money to transfer"}
                )
