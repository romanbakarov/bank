from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class BankAccount(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    balance = models.PositiveIntegerField()

    def make_transfer(self, other, amount):
        if self.balance - amount >= 0:
            self.balance -= amount
            other.balance += amount
            self.save(update_fields=['balance'])
            other.save(update_fields=['balance'])


class TransferHistory(models.Model):
    sender = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='receiver')
    amount = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']
