from bank_accounts import models


def write_transfer_history(sender, receiver, amount):
    models.TransferHistory.objects.create(
        sender=sender,
        receiver=receiver,
        amount=amount
    )
