from datetime import datetime

from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APITestCase, APIClient
from unittest.mock import patch

from bank_accounts import models

User = get_user_model()


class TransferAPITestCase(APITestCase):
    def setUp(self):
        self.customer_1 = User.objects.create(username='customer_1')
        self.customer_2 = User.objects.create(username='customer_2')
        self.bank_account_1 = models.BankAccount.objects.create(customer=self.customer_1, balance=1000)
        self.bank_account_2 = models.BankAccount.objects.create(customer=self.customer_2, balance=500)
        self.client = APIClient()

    def test_transfer_to_another_bank_account(self):
        self.client.force_authenticate(self.customer_1)
        data = {
            "sender": self.bank_account_1.pk,
            "receiver": self.bank_account_2.pk,
            "amount": 500
        }
        self.client.post('/api/make_transfer/', data=data)
        self.bank_account_1.refresh_from_db()
        self.bank_account_2.refresh_from_db()
        self.assertEqual(self.bank_account_1.balance, 500)
        self.assertEqual(self.bank_account_2.balance, 1000)

    def test_bank_balance_cannot_be_negative_after_transfer(self):
        self.client.force_authenticate(self.customer_1)
        data = {
            "sender": self.bank_account_1.pk,
            "receiver": self.bank_account_2.pk,
            "amount": 1100
        }
        response = self.client.post('/api/make_transfer/', data=data)
        self.assertEqual(response.status_code, 400)
        self.bank_account_1.refresh_from_db()
        self.bank_account_2.refresh_from_db()
        self.assertEqual(self.bank_account_1.balance, 1000)
        self.assertEqual(self.bank_account_2.balance, 500)

    def test_sender_bank_account_belongs_to_request_customer(self):
        self.client.force_authenticate(self.customer_1)
        data = {
            "sender": self.bank_account_2.pk,
            "receiver": self.bank_account_1.pk,
            "amount": 200
        }
        response = self.client.post('/api/make_transfer/', data=data)
        self.assertEqual(response.status_code, 400)
        self.bank_account_1.refresh_from_db()
        self.bank_account_2.refresh_from_db()
        self.assertEqual(self.bank_account_1.balance, 1000)
        self.assertEqual(self.bank_account_2.balance, 500)

    def test_cant_transfer_to_self_bank_account(self):
        self.client.force_authenticate(self.customer_1)
        data = {
            "sender": self.bank_account_1.pk,
            "receiver": self.bank_account_1.pk,
            "amount": 200
        }
        response = self.client.post('/api/make_transfer/', data=data)
        self.assertEqual(response.status_code, 400)
        self.bank_account_1.refresh_from_db()
        self.bank_account_2.refresh_from_db()
        self.assertEqual(self.bank_account_1.balance, 1000)
        self.assertEqual(self.bank_account_2.balance, 500)


class TransferHistoryAPITestCase(APITestCase):
    def setUp(self):
        self.customer_1 = User.objects.create(username='customer_1')
        self.customer_2 = User.objects.create(username='customer_2')
        self.bank_account_1 = models.BankAccount.objects.create(customer=self.customer_1, balance=1000)
        self.bank_account_2 = models.BankAccount.objects.create(customer=self.customer_2, balance=500)
        self.client = APIClient()

    @patch('django.utils.timezone.now')
    def test_transfer_history_created_at_transfer_api(self, mock_timezone):
        dt = datetime(2010, 1, 1, tzinfo=timezone.utc)
        mock_timezone.return_value = dt
        self.client.force_authenticate(self.customer_1)
        data = {
            "sender": self.bank_account_1.pk,
            "receiver": self.bank_account_2.pk,
            "amount": 500
        }
        self.client.post('/api/make_transfer/', data=data)
        self.assertEqual(models.TransferHistory.objects.last().sender, self.bank_account_1)
        self.assertEqual(models.TransferHistory.objects.last().receiver, self.bank_account_2)
        self.assertEqual(models.TransferHistory.objects.last().amount, 500)
        self.assertEqual(models.TransferHistory.objects.last().created_at, dt)
