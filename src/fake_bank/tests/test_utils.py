from django.test import TestCase
from django.contrib.auth import get_user_model
from bank_accounts import models, utils

User = get_user_model()


class TransferHistoryUtilsTestCase(TestCase):
    def setUp(self):
        self.customer_1 = User.objects.create(username='customer_1')
        self.customer_2 = User.objects.create(username='customer_2')
        self.bank_account_1 = models.BankAccount.objects.create(customer=self.customer_1, balance=1000)
        self.bank_account_2 = models.BankAccount.objects.create(customer=self.customer_2, balance=500)

    def test_write_transfer_history(self):
        utils.write_transfer_history(
            sender=self.bank_account_1,
            receiver=self.bank_account_2,
            amount=500
        )
        self.assertEqual(models.TransferHistory.objects.last().sender, self.bank_account_1)
        self.assertEqual(models.TransferHistory.objects.last().receiver, self.bank_account_2)
        self.assertEqual(models.TransferHistory.objects.last().amount, 500)
