from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from .swagger_view import schema_view


swagger_urlpatterns = [
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(), name='schema-json-yml'),
    path('swagger/', schema_view.with_ui('swagger'), name='schema-swagger-ui'),
]


urlpatterns = [
    path('administrator_panel/', admin.site.urls),
    path('api/', include('bank_accounts.urls'))
]

urlpatterns += swagger_urlpatterns
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
