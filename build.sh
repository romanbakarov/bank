GIT_BUILD_VERSION=$(git rev-parse --short HEAD)
n=${ECR_DOMAIN}/{{ project_name }}
echo LABEL BuildVersion=\"${GIT_BUILD_VERSION:-noversion}\" >>Dockerfile
docker build . -t $n
docker push $n
docker tag $n $n:${GIT_BUILD_VERSION}
docker push $n:${GIT_BUILD_VERSION}
docker rmi $n $n:${GIT_BUILD_VERSION}
echo ${GIT_BUILD_VERSION} > {{ project_name }}.txt