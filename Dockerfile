FROM python:3.8-alpine

RUN apk update && \
    apk add --virtual build-deps gcc python3-dev musl-dev && \
    apk add zlib-dev jpeg-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd && \
    apk add bash


RUN mkdir /home/fake_bank/
WORKDIR /home/fake_bank/
COPY src /home/fake_bank/
COPY .flake8 .pre-commit-config.yaml /home/fake_bank/
COPY requirements.txt /home/fake_bank/
RUN pip install -r requirements.txt
RUN chmod +x /home/fake_bank/docker-entrypoint.sh

ENTRYPOINT ["bash", "docker-entrypoint.sh"]
